#!/bin/zsh

###############################################################################
# AUXILIAR
###############################################################################
_get_start_time() {
	#start_time=$(ssh $env 'date +"%Y-%m-%d %H:%m:00"')
	start_time=$(date --date='11 hours ago' +"%Y-%m-%d %H:%m:%S")
}

_get_input_dir() {
	local_file='.dcpc.properties'
	rsync -qz $env:$install_path/dcpc/config/dcpc.properties $local_file
	dcpc_mode=$(cut -d' ' -f3 <(grep -E 'dcpc.mode' $local_file ))

	case $dcpc_mode in
		'distributor')
			dcpc_input_dir=$(grep -E '^file.reading.distributor' $local_file | cut -d'=' -f2 | sed 's/\s//g')
			;;
		'dual' | 'dcpc')
			dcpc_input_dir=$(grep -E '^file.reading.dir.dcpc' $local_file | cut -d'=' -f2 | sed 's/\s//g')
			;;
		*)
			echo ">ERROR> DcPc mode not recognised: $dcpc_mode" >&2
			;;
	esac

	# Take into account relative paths
	dcpc_input_dir=$( echo "${dcpc_input_dir}" | sed 's/^\./\/home\/user\/install\/dcpc/' )
	rm $local_file
}

_get_req_content() {
	req_content="$req_type,$target,$priority,$start_time$period$frequency,$timeout${specific_parameters:+,$specific_parameters}"
}


#############################
# REQUESTS
CONNECTIVITY_CHECK() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam_name" >&2
		exit 2
	fi
	target=$1
}

LINE_CARD() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam_name" >&2
		exit 2
	fi
	target=$1
}

MANAGE_PORT_STATUS() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1

	specific_parameters="ENABLE;DISABLE"
}

PER_TONE_DATA() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}


###############
# DSL
POP_O() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
	specific_parameters='US;DS;BITS'
}

BULK_POP_O() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam_name" >&2
		exit 2
	fi
	target=$1
	specific_parameters="US;DS;BITS${2:+;$2}"
}


POP_P() {
	if [[ $# < 1 ]] || [[ $# > 2 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id specific_parameters" >&2
		echo "       specific_parameters should always be provided starting with a ','"
		exit 2
	fi
	target=$1
	specific_parameters=$2
}

BULK_POP_P() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam_name" >&2
		exit 2
	fi
	target=$1
}

PROFILE_CHANGE() {
	if [[ $# != 2 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id new_profile" >&2
		exit 2
	fi
	target=$1
	new_profile=$2
	specific_parameters="UNKNOWN_OK;$new_profile"
}

BULK_VECTORING() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam_name" >&2
		exit 2
	fi
	target=$1
}

PORT_CONFIGURATION() {
	if [[ $# < 2 || $2 != (PM|CC|OT)(_GET_STATUS|_ENABLE|_DISABLE) ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id action [olt_id]" >&2
		echo ""
		echo "       Note: action shall be one of: [PM|CC|OT][_GET_STATUS|_ENABLE|_DISABLE]"
		echo "             Examples: PM_GET_STATUS, CC_ENABLE, OT_DISABLE"
		exit 2
	fi
	target=$1
	action=$2

	if [[ -n $3 ]]; then
		olt_id=";OLT_ID=$3"
	fi
	specific_parameters="ACTION=$action$olt_id"
}

BULK_PORT_CONFIGURATION() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam_name" >&2
		exit 2
	fi
	target=$1
}

BULK_PROFILE_COLLECTION() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam_name" >&2
		exit 2
	fi
	target=$1
}

BULK_TRAFFIC_INFO() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam_name" >&2
		exit 2
	fi
	target=$1
	shift
	specific_parameters=$@
}

SELT() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1

	# IS_OK = Whether to bring down the line or not if it's in service:
	#   - If present: Run always the SELT collection; this will always bring the line down if it's IS. Intended mainly for NAPI/GUI usage.
	#   - If not present (default): Run the SELT collection only if the line is not IS (it will fail if it's IS). Intended for dcpc nightly collections, in order to not bring down lines that are IS.
	#   - Only for SELT, not MELT
	specific_parameters="IS_OK"
}

MELT() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	req_type='XELT'
	target=$1

	specific_parameters="MELT"
}


###############
# PON
PON_ALL() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id ([; PON_POP_O] [ ; PON_POP_P ] [ ; PON_ONT_INFO ] [ ; PON_OLT_INFO ] [ ; PON_ALARM ] [ ; CURRENT ] [ ; HISTORY ] [ ; HISTORY_HOURS ] [ ; LAYER2_DATA ] [ ; PON_LINE_STATUS ])" >&2
		exit 2
	fi
	target=$1

	specific_parameters="$2"
}

BULK_PON_ALL() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam" >&2
		exit 2
	fi
	target=$1
}

PON_POP_O() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}

BULK_PON_POP_O() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam" >&2
		exit 2
	fi
	target=$1
}

PON_POP_P() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}

BULK_PON_POP_P() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}


PON_ONT_INFO() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}

PON_OLT_INFO() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}

PON_ALARM() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}

BULK_PON_ALARM() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam" >&2
		exit 2
	fi
	target=$1
}

LAYER2_DATA() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}

BULK_LAYER2_DATA() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam" >&2
		exit 2
	fi
	target=$1
}

PON_LINE_STATUS() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}

BULK_PON_LINE_STATUS() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam" >&2
		exit 2
	fi
	target=$1
}


MANAGE_ONT_STATUS() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id action ([ ENABLE ] [ ; DISABLE ] [ ; REMOTE_RESET ] [ ; COLLECT_BEFORE ] [ ; COLLECT_AFTER ][ ; PORT_TYPE=[ONT|ONT_CARD|HSI|ETHERNET] ])" >&2
		echo ""
		echo "       Note: action is one of: ENABLE, DISABLE, REMOTE_RESET"
		exit 2
	fi
	target=$1
	action=$2
	
	specific_parameters="$action${3:+;$3}"
}

MANAGE_OLT_STATUS() {
	if [[ $# < 2 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id olt_id ([; ENABLE ] [ ; DISABLE ])" >&2
		echo ""
		echo "       Note: olt_id is the part on the left of the # symbol in V_PORTS.PORTS,"
		echo "             no matter if that is X-X-X or X-X-X-X"
		exit 2
	fi
	target=$1
	olt_id=$2

	specific_parameters="OLT_ID=$olt_id${3:+;$3}"
}

# GHN
GHN_POP_O() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}


GHN_POP_P() {
	if [[ $# < 1 ]] || [[ $# > 2 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id specific_parameters" >&2
		echo "       specific_parameters should always be provided starting with a ','"
		exit 2
	fi
	target=$1
	specific_parameters=$2
}

GHN_ALARM() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}

BULK_GHN_ALARM() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam" >&2
		exit 2
	fi
	target=$1
}

GHN_LINE_STATUS() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}

GHN_VENDOR_INFO() {
	if [[ $# != 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1
}


GHN_ALL() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id ([; GHN_POP_O] [ ; GHN_POP_P ]  [ ; GHN_ALARM ] [ ; CURRENT ] [ ; HISTORY ] [ ; HISTORY_HOURS ] [ ; GHN_LINE_STATUS ])" >&2
		exit 2
	fi
	target=$1

	specific_parameters="$2"
}

BULK_GHN_ALL() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam" >&2
		exit 2
	fi
	target=$1
}

# HSC
HSC_TRAFFIC_OLT() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam" >&2
		exit 2
	fi
	target=$1

	specific_parameters="$2"
}

HSC_TRAFFIC_ONT() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1

	specific_parameters="$2"
}

HSC_OPERATIONAL_OLT() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) dslam" >&2
		exit 2
	fi
	target=$1

	specific_parameters="$2"
}

HSC_OPERATIONAL_ONT() {
	if [[ $# < 1 ]]; then
		echo ">ERROR> Syntax is: dcpc.sh [-env env.ironm.ent] $(basename $0) line_id" >&2
		exit 2
	fi
	target=$1

	specific_parameters="$2"
}


###############################################################################
# MAIN
###############################################################################

#############################
# 1. Parse input
zparseopts -F -D -help=o_help -verbose=o_verbose -env:=o_env -dry-run=o_dryrun -install-path:=o_install_path -rt=o_realtime -crumb=o_crumb
env="${o_env[2]:-$CUSTOMER_ENVIRONMENT}"
install_path="${o_install_path[2]:-install}"
request_path=${o_crumb:+.}
if [[ $o_verbose ]]; then echo ">DEBUG> request_path=$request_path" >&2 ; fi

if [[ ! -z $o_help ]]; then
	echo $0
	echo "$(grep -oE '^[^[:blank:]]+\(' $0 | sed 's/(//')" >&2
	exit
fi

if [[ $# < 1 ]]; then
	echo ">ERROR> Syntax is: dcpc.sh [--env env.ironm.ent] [--dry-run] [--install-path path] [--rt] [--crumb] request_type target [specific request parameters]\n" >&2
	echo "        [--dry-run] do everything but send the request" >&2
	echo "        [--install-path path] override Expresse install path (it usually is /home/user/install)" >&2
	echo "        [--rt] send realtime request, that is priority=9" >&2
	echo "        [--crumb] write the request file being sent into current directory" >&2
	exit 2
fi

# TODO: allow overriding from input
if [[ -n ${o_realtime[1]} ]]; then
	priority=9
else
	priority=5
fi
timeout=-1

#############################
# 2. Send request
# Does the user want to send a file?
if [[ -a $1 ]]; then
	if [[ $o_verbose ]]; then echo ">DEBUG> Sending file $1" >&2 ; fi
	req_type=${1%.dat}
	req_content=$( cat $1 )
	if [[ $o_verbose ]]; then echo ">DEBUG> Its content is: $req_content" >&2 ; fi
# Else he must be requesting us to build the request for him
else
	# Converting to uppercase (Check 14.3.1 Parameter Expansion Flags in ZSH manual)
	req_type=${(U)1}
	shift 
	# Request specifics. Exit if request type is not implemented
	$req_type $@ || exit 1
	_get_start_time
	_get_req_content
fi

# In case req_type is the path to a temporary file, because of using shell's file temporary creation
# we remove the leading
request_file="${request_path:-/tmp}/${req_type##*/}.dat"
if [[ $o_verbose ]]; then echo ">DEBUG> req_type=$req_type && request_file=$request_file && req_content=$req_content" >&2 ; fi
echo $req_content > $request_file
_get_input_dir

if [[ -z $o_dryrun ]]; then
	echo ">INFO> Sending $request_file to $env:$dcpc_input_dir" >&2
	rsync -qvh $request_file $env:$dcpc_input_dir
else
	echo ">INFO> Not sending $request_file to $env:$dcpc_input_dir" >&2
fi
