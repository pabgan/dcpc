#!/bin/zsh

##########################
# 0. PARSE INPUT
if [[ $# < 1 ]]; then
	echo "ERROR: Syntax is: $(basename $0) file_path [sed options]"
	exit 2
fi

sed -E 's/([^=]collect[^=]+=\s+)false/\1true/' $@
