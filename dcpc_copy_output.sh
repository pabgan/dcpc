#!/bin/zsh

###############################################################################
# AUXILIAR
###############################################################################

# TODO: allow overriding from input
get_output_dir() {
	local_file=/tmp/dcpc.properties
	rsync --quiet --compress ${env}:~/install/dcpc/config/dcpc.properties ${local_file}
	dcpc_mode=$(cut --delimiter=' ' --fields=3 <(grep -E 'dcpc.mode' $local_file ))
	if [[ $o_verbose ]]; then echo ">INFO> DcPc mode is ${dcpc_mode}" >&2 ; fi

	case ${dcpc_mode} in
		'distributor')
			if [[ $o_verbose ]]; then echo ">WARN> Distributor mode not implemented" >&2 ; fi;
			exit 2;
			;;
		'dual' | 'dcpc')
			if [[ $o_verbose ]]; then echo ">DEBUG> Getting output dir for dcpc mode" >&2 ; fi;
			status_codes_output_dir=$(grep -E '^file.statuscode.dir.dcpc' $local_file | cut -d'=' -f2 | sed 's/\s//g');
			result_output_dir=$(grep -E '^file.output.dir.dcpc' $local_file | cut -d'=' -f2 | sed 's/\s//g');
			temp_result_output_dir=$(grep -E '^file.temp.output.dir.dcp' $local_file | cut -d'=' -f2 | sed 's/\s//g');
			if [[ $o_verbose ]]; then echo ">DEBUG> DcPc configured to drop files in ${status_codes_output_dir} and ${temp_result_output_dir}, to then transfer them to ${result_output_dir}" >&2 ; fi;
			;;
		*)
			echo ">ERROR> DcPc mode not recognised: ${dcpc_mode}" >&2;
			exit 2;
			;;
	esac

	# Take into account relative paths
	if [[ ${status_codes_output_dir} =~ "^\./" ]]; then
		echo ">WARN> file.statuscode.dir.dcpc es una ruta relativa." >&2
		# See page 49 in ZSH manual
		# The pattern may begin with a ‘#’, in which case the pattern
		# must match at the start of the string
		status_codes_output_dir=${status_codes_output_dir/#./install\/dcpc}
	fi

	if [[ ${result_output_dir} =~ "^\./" ]]; then
		echo ">WARN> file.output.dir.dcpc es una ruta relativa." >&2
		# See page 49 in ZSH manual
		# The pattern may begin with a ‘#’, in which case the pattern
		# must match at the start of the string
		result_output_dir=${result_output_dir/#./install\/dcpc}
	fi

	if [[ ${temp_result_output_dir} =~ "^\./" ]]; then
		echo ">WARN> file.temp.output.dir.dcpc es una ruta relativa." >&2
		# See page 49 in ZSH manual
		# The pattern may begin with a ‘#’, in which case the pattern
		# must match at the start of the string
		temp_result_output_dir=${temp_result_output_dir/#./install\/dcpc}
	fi
}


###############################################################################
# MAIN
###############################################################################

#######################################
# 0. Parse input
zparseopts -F -D -verbose=o_verbose -env:=o_env
env="${o_env[2]:-$CUSTOMER_ENVIRONMENT}"

if [[ $# > 0 ]]; then
	echo "ERROR: Syntax is: $(basename $0) [-env env.ironm.ent]"
	exit 2
fi

#######################################
# 2. Do da thing
get_output_dir
temp_dir=$(mktemp -d)
while true; do
	rsync --delete -qrvh ${env}:$(echo ${status_codes_output_dir}) :${temp_result_output_dir} :${result_output_dir} ${temp_dir}
	find ${temp_dir} -type f -exec cp {} . \;;
	clear
	ls *.dat $.txt 2> /dev/null
	sleep 1
done
